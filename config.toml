baseurl = "https://learnings.desipenguin.com"
languageCode = "en"
defaultContentLanguage = "en"                # en / zh-cn / ... (This field determines which i18n file to use)
title = "Learn. Share. Improve"
# preserveTaxonomyNames = true
enableRobotsTXT = true
enableEmoji = true
theme = "jane"
enableGitInfo = true # use git commit log to generate lastmod record

# Syntax highlighting by Chroma. NOTE: Don't enable `highlightInClient` and `chroma` at the same time!
pygmentsOptions = "linenos=table"
pygmentsCodefences = true
pygmentsUseClasses = true
pygmentsCodefencesGuessSyntax = true

hasCJKLanguage = false     # has chinese/japanese/korean ?
paginate = 7

# No one uses comments these days, at least make the page lighter for users.
disqusShortname = "" # Original value "wiltw"
googleAnalytics = "UA-73794283-1"
copyright  = "Mandar Vaze"

[author]                  # essential
  name = "Mandar Vaze"

[sitemap]                 # essential
  changefreq = "weekly"
  priority = 0.5
  filename = "sitemap.xml"

[[menu.main]]             # config your menu
  name = "Home"
  weight = 10
  identifier = "home"
  url = "/"
[[menu.main]]
  name = "Galleries"
  weight = 15
  identifier = "galleries"
  url = "/galleries/"
[[menu.main]]
  name = "Archives"
  weight = 20
  identifier = "archives"
  url = "/post/"
[[menu.main]]
  name = "Work with me"
  weight = 55
  identifier = "freelance"
  url = "/freelance/"

[params]
  debug = false             # If true, load `eruda.min.js`. See https://github.com/liriliri/eruda

  since = "2013"            # Site creation time          # 站点建立时间
  # use public git repo url to link lastmod git commit, enableGitInfo should be true.
  # 指定 git 仓库地址，可以生成指向最近更新的 git commit 的链接，需要将 enableGitInfo 设置成 true.
  gitRepo = "https://bitbucket.org/mandarvaze/mandarvaze_bitbucket_blog_src"

  # site info (optional)                                  # 站点信息（可选，不需要的可以直接注释掉）
#  keywords = ["Hugo", "theme","even"]
#  description = "Hugo theme even example site."

  # paginate of archives, tags and categories             # 归档、标签、分类每页显示的文章数目，建议修改为一个较大的值
  archivePaginate = 5

  # show 'xx Posts In Total' in archive page ?            # 是否在归档页显示文章的总数
  showArchiveCount = false

  # The date format to use; for a list of valid formats, see https://gohugo.io/functions/format/
  dateFormatToUse = "Jan 02, 2006"

  # show word count and read time ?                       # 是否显示字数统计与阅读时间
  moreMeta = true

  # Syntax highlighting by highlight.js
  # highlightInClient = false

  # 一些全局开关，你也可以在每一篇内容的 front matter 中针对单篇内容关闭或开启某些功能，在 archetypes/default.md 查看更多信息。
  # Some global options, you can also close or open something in front matter for a single post, see more information from `archetypes/default.md`.
  toc = true                                                                            # 是否开启目录
  autoCollapseToc = true   # Auto expand and collapse toc                              # 目录自动展开/折叠
  fancybox = true          # see https://github.com/fancyapps/fancybox                 # 是否启用fancybox（图片可点击）

  # mathjax
  mathjax = false           # see https://www.mathjax.org/                              # 是否使用mathjax（数学公式）
  mathjaxEnableSingleDollar = false                                                     # 是否使用 $...$ 即可進行inline latex渲染
  mathjaxEnableAutoNumber = false                                                       # 是否使用公式自动编号
  mathjaxUseLocalFiles = false  # You should install mathjax in `yout-site/static/lib/mathjax`

  postMetaInFooter = false  # contain author, lastMod, markdown link, license           # 包含作者，上次修改时间，markdown链接，许可信息
  linkToMarkDown = true     # Only effective when hugo will output .md files.           # 链接到markdown原始文件（仅当允许hugo生成markdown文件时有效）
  contentCopyright = ''     # e.g. '<a rel="license noopener" href="https://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank">CC BY-NC-ND 4.0</a>'

  # Link custom CSS and JS assets
  #   (relative to /static/css and /static/js respectively)
  customCSS = ['custom.css']
  customJS = []

  uglyURLs = false          # please keep same with uglyurls setting

  [params.publicCDN]        # load these files from public cdn                          # 启用公共CDN，需自行定义
    enable = true
    jquery = '<script src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>'
    slideout = '<script src="https://cdn.jsdelivr.net/npm/slideout@1.0.1/dist/slideout.min.js" integrity="sha256-t+zJ/g8/KXIJMjSVQdnibt4dlaDxc9zXr/9oNPeWqdg=" crossorigin="anonymous"></script>'
#    gitmentJS = '<script src="https://cdn.jsdelivr.net/npm/gitment@0.0.3/dist/gitment.browser.min.js" crossorigin="anonymous"></script>'
#    gitmentCSS = '<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/gitment@0.0.3/style/default.min.css" crossorigin="anonymous">'
    photoswipe = '<script src="https://cdn.jsdelivr.net/npm/photoswipe@4.1.3/dist/photoswipe.js" integrity="sha256-AC9ChpELidrhGHX23ZU53vmRdz3FhKaN9E28+BbcWBw=" crossorigin="anonymous"></script>'
    photoswipeUI = '<script src="https://cdn.jsdelivr.net/npm/photoswipe@4.1.3/dist/photoswipe-ui-default.min.js" integrity="sha256-UKkzOn/w1mBxRmLLGrSeyB4e1xbrp4xylgAWb3M42pU=" crossorigin="anonymous"></script>'
    photoswipeCSS = '<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/photoswipe@4.1.3/dist/photoswipe.css" integrity="sha256-SBLU4vv6CA6lHsZ1XyTdhyjJxCjPif/TRkjnsyGAGnE=" crossorigin="anonymous">'
    photoswipeSKIN = '<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/photoswipe@4.1.3/dist/default-skin/default-skin.css" integrity="sha256-c0uckgykQ9v5k+IqViZOZKc47Jn7KQil4/MP3ySA3F8=" crossorigin="anonymous">'

  [params.social]                                         # 社交链接
    b-stack-overflow = "https://stackoverflow.com/users/154947/mandar-vaze"
    c-twitter = "https://twitter.com/mandarvaze"
    e-linkedin = "https://www.linkedin.com/in/mandarvaze"
    g-github = "https://github.com/mandarvaze"
    m-instagram = "https://www.instagram.com/mandar_vaze/"
    n-gitlab = "https://gitlab.com/mandarvaze"
