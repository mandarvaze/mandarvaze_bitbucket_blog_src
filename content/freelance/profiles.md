---
title: "Links to Various Online Profiles"
date: 2020-04-14T11:32:52+05:30
lastmod: 2020-04-14T11:32:52+05:30
keywords: ["freelance", "consulting" , "startups"]
menu: "Work with me"
toc: true
---

I am available for remote freelance assignments.

# Profile Links

* [Github](https://github.com/mandarvaze/)
* [Twitter](https://twitter.com/mandarvaze)
* [Linkedin](https://www.linkedin.com/in/mandarvaze)
* [Stackoverflow](https://stackoverflow.com/users/154947/mandar-vaze)
* [Digital Moleskine](https://wiki.desipenguin.com)
