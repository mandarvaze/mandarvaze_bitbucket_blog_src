---
title: "Project : Image Analytics for Logistics Company"
date: 2020-03-05T18:37:11+05:30
tags: ["freelance"]
categories: ["clients"]
keywords: ["google cloud vision", "OCR", "API service", "RapidAPI"]
description: "Simplify processing Proof of Delivery for Logistics Company"

comment: false
toc: true
autoCollapseToc: false
---
Simplify processing Proof of Delivery for Logistics Company

<!--more-->

**Client : Stealth mode startup in Logistics Domain**

### Features ###

* In its early form, the product is just couple of APIs
  * First API accepts publicly accessible image file URL, and returns the
    POD number
  * Stats API returns various data related to past API calls.
* Multi-tenant support since each customer has different POD format.
* Support for multiple POD formats for single customer.
* Product available as APIs that can be integrated into customer's existing system.
* APIs available on the RapidAPI market place.
* Pay-per-use pricing

### Technology ###

* Backend : Python 3, FastAPI for REST API server, Google Vision API
  * Testing : pytest
  * Documentation : FastAPI, Swagger, OpenAPI
* Database : Postgres
* PaaS : Heroku
* Marketplace : RapidAPI

### My Role ###

As **An Architect and the Back-end Engineer**, I was the and only engineer on
the development team.

I worked with the founders and Domain experts to understand the requirements.

The potential customer already had their systems in place, and they only wanted
to simplify processing the POD

Both FastAPI (my choice) and RapidAPI market place (Business Choice) was new for
me. I had great time learning the FastAPI framework and exploring the RapidAPI.

### Design considerations and Choices ###

OCR is the critical component of the product. We considered options like
Tesseract in the beginning, but decided to use Google Cloud Vision to better
Developer productivity. It is easier to use and no setup required. The plan was
to replace it some library that the business would not have to pay for.

In past, I have worked (and deployed in production) Eve framework, which is
based on top of Flask. But that was couple years ago.

This time, I chose FastAPI for the developer productivity and for its async
support. Use of Pydantic was very interesting and helpful.

Initially, there was no database since we assumed RapidAPI would provide the
Stats about the API calls. But it turned out to be limiting. Additionally, the
Stats provided by RapidAPI were available only from their Dashboard, and were
(ironically) not accessible via API. So we decided to provide our own API

### Project Duration ###

Dec 2019 - Feb 2020

### Related Blog Posts ###

* [How to Process Headers using FastAPI](/post/headers-fastapi/)
* [How to Write Middleware using FastAPI](/post/middleware-fastapi/)
