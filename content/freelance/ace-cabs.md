---
title: "Project: Corporate Taxi Hiring platform"
date: 2018-12-24T18:36:39+05:30
keywords: ["taxi", "cab", "online", "heroku", "postgres"]
description: "Corporate Taxi Hiring platform"
tags: ["freelance"]
categories: ["clients"]

comment: false
toc: true
autoCollapseToc: false
---
Corporate Taxi Hiring platform.
<!--more-->

**Client : Ace Cabs India Pvt. Ltd.**

![Ace Cabs](https://web.archive.org/web/20161103104319im_/http://acecabs.in/images/logo.png)

### Features ###

* GPS enabled billing
* Instant Bill on SMS and Email
* mini map of the route on the Invoice
* Support for Multiple Operators (Multi Tenant Support)
* Support for different tariff Per Vehicle Type, Per City, Per Client
* Support for multiple billing entities for single Corporate Client
* Support for different GST rates per state (CGST, SGST, IGST)

### Revenue ###

Within 1 year of operation, we generated :

* 10000+ Invoices
* **Rs. 1.8 Cr** revenue

### Technology ###

* Backend : Python 3, flask (eve framework) for REST API server
  * Testing : pytest
  * Documentation : Sphinx
* Database : Postgres
* PaaS : Heroku
* Android : Java
* Web : Angular

### My Role ###

As **An Architect and the Back-end Engineer**, I was the first person to join
the project.

I met with the stake holders to understand the requirements.
We jointly created the road map.

I had to create enough API endpoints upfront for the front end and Android
development to start.

Unit and integration tests came in handy to ensure that I could test the
functionality independently.

I later deployed the server to heroku. We had considered AWS, but Heroku is best
for the developer productivity, Especially when you don't have a dedicated
DevOps person.

I took regular backups via `pg_dump`

### Design considerations and Choices ###

While we were finalizing other details, I had already started my background
work. The project was different from other products in OneDelta that Android
client was going to center piece. So we needed to use API server model, rather
than Server Rendered pages approach that worked earlier.

I considered Web2py - which was used in previous projects, but decided that
web2py shines elsewhere (even though it supports creating REST endpoints)

I had also considered DRF, but found it too confusing (and restrictive).

Finally, settled with Eve framework, which is built on top of Flask. Out of the
box, Eve supports mongoDB, but support for RDBMS was available via third party
plugin. (The plugin wasn't maintained well, luckily we were never blocked by any
of the issues. I was always able to figure my way out.)

Since most important data that we would collect would be GPS co-ordinates, I had
considered mongoDB for a while. But my "research" lead me to decide it is not
worth the trouble. Plus rest of the data was of relational nature.
So I selected Postgres.

GPS data was stored as `Column(MutableDict.as_mutable(JSONEncodedDict))`
This is internally stored as `VARCHAR`

I had considered `HSTORE` but since we needed to update the field a LOT (every 30
seconds or so) performance would impact.

### Real world Fun Facts ###

* This was a traditional corporate taxi company, where the drivers were not
trained to use the app to start/stop the trips, so they would forget ;) (Ask
me in person, how we "handled" it)
* The trips lasted 8-12 hours, where drivers - when on a break - would walk with
the phone, causing erroneous readings.
* Various parts of town would have patchy or non existent cell coverage.
* GST was introduced after 6 months in production
* Demonetization was announced the night I was flying to Delhi for Go-Live testing.

### Project Duration ###

Aug 2016 - Feb 2018
