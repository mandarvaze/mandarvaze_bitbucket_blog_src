---
title: "Product: Complius - Legal Compliance Management"
date: 2016-07-27T08:20:54+05:30
keywords: ["legal-tech", "legal", "compliance"]
description: "Legal Compliance Management"
tags: ["freelance"]
categories: ["clients"]

comment: false
toc: true
autoCollapseToc: false
---
Legal Compliance Management.

<!--more-->

**Client : OneDelta Synergies Pvt. Ltd.**

<!-- Does not look good on white background
![OneDelta](https://onedelta.in/wp-content/themes/onedelta/images/logo.png)
-->

### Features ###

* At the heart of Complius lies the engine that determine which of over tens of
  thousand laws apply to your company/location.
* Send periodic reminders to the owner with upcoming compliance tasks
* Sends reports of compliance (and non compliance) to the management
* Ability to upload proofs
* Proofs are verified by team of legal experts
* Additional details on the [Product
  Page](https://onedelta.in/product/complius/)
  
### Revenue ###

Within first three years :

* Managed 10,000+ legal compliance activities across India
* Managed 500+ Locations across 80 Companies. 
* **Rs. 1.5 Cr** revenue

### Technology ###

* Web2py full stack framework
* Database: MySQL
* Hosting Provider : Webfaction

### My Role ###

As founding Engineer, I worked with the legal domain experts to define the
product. Compliance being new area, I had to work closely with legal team to
understand various nuances of how laws are applicable. This was eventually coded
into the compliance task generation engine.

The engine was a standalone script that would run once at the beginning, when a
company is "on-boarded" and then beginning of each calendar year.

POC was show cased to the stakeholders within 4 months, and after initial
feedback was incorporated, the first company was on boarded and complius "went
live". 

Once the revenue started flowing, we hired additional team members to develop
additional features, one full time devops cum (internal) customer support person
was hired. (Till then I was handling both those aspects along with mentoring
newer team members)

As we started having more and more compliance tasks in the system, the UI
dashboard started slowing down. I was able to "improve" the performance from 10
seconds (Gasp!) to under a second by optimizing a query.

Code change was (obviously) tiny, but finding the "bug" was challenging and fun.

### Real world fun facts ###

* This was a new market we were trying to break into.
* Sometimes (non legal) team members would question "does it even matter"
* Legal team would eventually get "bored" checking the proofs
* We spent a good deal building a "Dashboard" and making it Fancy
  * Turns out, no one looked at it anyway.
* The "users" were reluctant to "login to the system"
* They loved the email reminders. (which made them log in to do further work.
  See previous point)
* We had considered a workflow were users can "send" in the proofs via email
  (It never boiled up to priority, at least till I was around anyway.)

### Project Duration ###

July 2012 - Aug 2016
