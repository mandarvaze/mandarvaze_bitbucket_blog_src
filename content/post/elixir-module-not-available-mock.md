---
title : "Elixir : module not available"
slug : "elixir-module-not-available-mock"
date : "2020-04-15T19:18:30+05:30"
categories : ["TIL"]
tags : ["elixir", "mock", "HTTPoison"]
summary: How I resolved *not-so-obvious* error `(UndefinedFunctionError) function HTTPoison.request/5 is undefined (module HTTPoison is not available`
---

## Problem ##

I have unit tests and integration tests

Both call the same set of functions.

All the integration tests passed. 🎉

All the unit tests failed. 😢

For every unit test , I got the same error :

``` shell
** (UndefinedFunctionError) function HTTPoison.request/5 is undefined (module HTTPoison is not available)
```

Searching for `module not available` pointed to solutions that were not
applicable. Most suggested including `httpoison` in `mix.exs` - which was
already taken care of.

I'm grateful for the people on the elixir forum for being helpful and
responsive. (You can read the details on elixir forum. Link at the end.)

## Root Cause ##

I am using `Mock` library in the unit tests. It turns out it changes what is
available to the underlying code (as it should).

I was using `HTTPoison.post` in the unit tests, while one of the lower level
function was calling `HTTPoison.request`

## Solution ##

The fix was as simple as changing `HTTPoison.post` to `HTTPoison.request` in the
`with_mock HTTPoison` block.

----

Reference : See the discussion on [Elixir Forum](https://elixirforum.com/t/undefinedfunctionerror-function-httpoison-request-5-is-undefined-module-httpoison-is-not-available/30546?u=mandarvaze)
