---
title: "Using ForeignKey in Abstract Base Class (SQLAlchemy)"
slug: "fk_abstract_class_sqlalchemy"
date: "2016-01-20"
categories: ["how-to"]
tags: ["python", "sqlalchemy"]
---

I was creating an abstract base class containing FK, like this :

```python
class MyBase(Base):
  name = Column(String(30))
  fk1 = Column(Integer, ForeignKey('table1.id'))
```

but kept getting the error :

```shell
sqlalchemy.exc.InvalidRequestError:
Columns with foreign keys to other columns must be declared as @declared_attr callables on declarative mixin classes.
```

Reading thru the docs was not helpful because the example used for
`@declared_attr` is somewhat different

[this SO answer](http://stackoverflow.com/a/32344269/154947) helped.

Essentially use it as :

```python
class MyBase(Base):
  name = Column(String(30))

  @declared_attr
  def fk1(cls):
      return Column(Integer, ForeignKey('table1.id'))
```
