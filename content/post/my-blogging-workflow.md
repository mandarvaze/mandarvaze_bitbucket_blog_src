---
title : "How to blog entirely from within Emacs : Hugo and Markdown"
slug : "my-blogging-workflow"
date : "2022-01-02T13:45:14+05:30"
categories : ["how-to", "meta"]
tags : ["emacs", "hugo"]
---

Last couple of weeks, specifically since I started my [microblog](https://microblog.desipenguin.com) I am able to do all the operations from within emacs.
<!--more-->

* Create new post via `hugo new post/my-new-post.md`
  * I do this by opening a `vterm` in emacs.
* Open the newly created post in Emacs via `SPC f f`
* Add content.
* Run dev server in `vterm` via `hugo server --disableFastRender`
* Preview and update content as required.
  * Earlier I used to preview in external browser, breaking my everything-from-emacs flow, till I configured Emacs to use `eww` to browse the links.
  * Read about it [here](https://microblog.desipenguin.com/post/eww-browser-in-emacs/)
* When done, add the file to git using `magit` via `SPC g g` (Go to Untracked files and <kbd>s</kbd> to stage the file.)
* git commit via `SPC g c c`
* git push via `M-x magit-push-matching`
* Optionally stop the development server started in `vterm`

Once I've pushed the new post to git, netlify automatically builds and publishes the post 🎉

