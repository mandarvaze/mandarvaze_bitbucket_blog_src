---
title: "Sidney Sheldon"
slug: "sidney-sheldon"
date: "2017-02-12"
tags: ["book", "people", "biography"]
categories: ["misc"]
---

I had known Sydney Sheldon as an author of several best-selling novels
like "The Other Side of Midnight", "Rage of Angels", "Master of the
Game" etc. I grew up reading his novels (mostly marathi translations)

Later, during my few years in US, I used to watch "I Dream of Jeanie"
and came to know him as the producer and writer of this show.

Little did I know, that I had all this backwards. Literally.

Recently I read his autobiography "The Other Side of Me". It turns out
he had a huge career in Hollywood and Broadway before that. Later in
life, he did television, and lastly a novelist.

The book starts with his thoughts of suicide when nothing was going
right for him. This was during the peak of "great depression". His
father talked him out of it. (He referred to both his parents by their
first name)

He later reveals, that later in his life (After he won an Oscar) when he
visited a psychiatrist, that he suffered from "Manic Depression", and
"1 out of 5" people suffering from this end up taking their own life.
(Sydney Sheldon lived till the age of 89, just 12 days short of his 90th
birthday)

During his initial days he worked in a pharmacy, in check room of a
hotel, and struggled as a song writer. He got his break, writing plays.

During the World War 2, he joined the armed forces and was trained to be
a pilot. He was never sent to the active war zone due to his slip disk
problem. He continued his work on Broadway.

At one point, he had three musicals on Broadway.

This eventually lead to his entry in Hollywood. He won an Oscar for his
first screenplay.

He worked with several leading men and women of those days.

He eventually became a producer, worked with several leading movie
studios of the time.

During the initial days of Television, people from Hollywood considered
Television as their rival, and wouldn't do TV. Sheldon was (probably)
one of the first people to move to Television. (He had almost decided
**not** to do television, till he met Patty Duke, the star of the proposed
Sitcom, over lunch, just as a courtesy)

Towards the end of the book, he says that he had a plot that was
"begging to be written" but thought that it would not be a good
material for visual medium like movie or television, since he wanted to
share the "thoughts" of the leading character with the audience. That
is how he become a novelist, and then wrote several novels and became
best selling author.

The biography ends with publishing of his first novel.

In the last chapter of his book, he talks about few interesting
experiences he had when doing research for his novels.

Just like his other books, it was a great read.
