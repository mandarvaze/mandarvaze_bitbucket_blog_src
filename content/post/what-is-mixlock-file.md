---
title : "What is mix.lock file"
date  : "2017-10-09"
tags  : ["elixir"]
categories : ["TIL"]
slug : what-is-mixlock-file
description: "At first I thought, it is a lock file like process lock, but it's not."
---


In past, I have seen several processes create `.lock`
files, to usually write `pid` of the process.

e.g. Emacs desktop package creates `.emacs.desktop.lock`
file to save the PID of the emacs instance that created this file.
Eventually, it will use this to ensure that no other instance overwrites
it, and also gives a warning as such.

There have been other instances we well.

So when I started working on elixir, I always ignored
`mix.lock` file. When I refer to other peoples elixir
repo on github, I always wondered why are they committing this file to
git.

I have seen junior/inexperienced developers committing *everything* to
git, including files which are built. So I ignored such "mistakes"

How naive I was.

Recently when I started working with phoenix, I bothered looking at the
`.gitignore` file. I was surprised that
`mix.lock` was *not* in it.

People who created phoenix framework are smart.

If they are smart enough to create a "default"
`.gitignore` file, they **know** what they want in there.

So I looked inside the `mix.lock` file, and realized that
it is definitely *not* PID :)

I should have thought about it earlier, since `mix` is
*not* a long running/daemon process. So why would it need to store the
PID ?

On reading the file, I realized that it is equivalent of
`requirements.txt` files from the python world, but
automatically created !

The documentation specifically asks to commit this file to version
control !

Here is the relevant part of the documentation:

> Dependency fetching is repeatable, Mix will lock the version of a
> dependency in the lockfile to ensure that all developers will get the
> same version (always commit mix.lock to version control). `$ mix
> deps.update` will update the dependency and write the updated version
> to the lockfile.

You can read up the original/details [here](https://hex.pm/docs/usage)

------------------------------------------------------------------------

Turns out python is also moving towards similar `.lock`
file mechanism over `requirements.txt`. See
[this](https://github.com/kennethreitz/pipenv).
