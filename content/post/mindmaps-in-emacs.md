---
title: "Mindmaps in Emacs"
date: 2022-11-24T09:35:27+05:30
slug: "mindmaps-in-emacs"
categories : ["how-to"]
tags: ["emacs", "mindmaps"]
summary: One of the thing I discovered this week is ability to create mindmaps  without leaving the comfort of Emacs 😄 This is made possible via PlantUML.
---

I have been using Emacs for about 4+ years now, and I still find new things.

One of the thing I discovered this week is ability to create mindmaps 
without leaving the comfort of Emacs 😄
This is made possible via PlantUML.

Doom emacs makes it very easy to set it up.

1. Enable `plantuml` in the `init.el` of your doom emacs config.
2. `M-x doom/reload` (This will install the appropriate packages)
3. `M-x plantuml-download-jar` (Make sure you have working java installation)
4. `doom/open-scratch-buffer`
5. `org-mode` (Not sure if this is needed, but better be explicit)

Now create following in your scratch buffer that is already in `plantuml` mode.

```org
#+begin_src plantuml :file my-diagram.png
@startmindmap
+ OS
++ Ubuntu
+++ Linux Mint
+++ Kubuntu
+++ Lubuntu
+++ KDE Neon
++ LMDE
++ SolydXK
++ SteamOS
++ Raspbian
-- Windows 95
-- Windows 98
-- Windows NT
--- Windows 8
--- Windows 10
@endmindmap
#+end_src
```

The above is called `Arithmatic Syntax` (due to `+` and `-`) benefit of this
is that `+` appear on right side of the main node and `-` appear on the left
side.

So if you prefer certain nodes to appear on the left vs right this is syntax
you should use.

If you don't care, then standard markdown syntax also works. Just that all the
nodes appear on the right side.

Now, `C-c C-e h o` to export your org document into html. We are interested in
`my-diagram.png` which will be in the same directory as your exported html file.

-----

If you do not want to export the file, then get rid of the first and last line
in the org file above (ones with `_src`) then `plantuml-mode` and `C-c C-c` to
preview the mindmap within emacs itself.

This is how it looks

![mindmap](/images/mindmap.png) 

-------

*Originally published on microblog [here](https://microblog.desipenguin.com/post/mindmaps-in-emacs/)*
