---
title: "Writing Pelican blog post using Nikola new_post"
date : 2017-06-16
tags : ['logging', 'nikola']
categories : ['meta']
---
*This post is ported over from my other blog, which was once in `pelican`*

<!--more-->

In my [last
post]({{< ref "post/support-for-pelican-style-metadata-format-in-nikola.md" >}}),
I mentioned that Nikola can use the pelican/resT docinfo style metadata.

I had also mentioned that, now can I use the same post in both the
places without any change. But if I used nikola's
`new_post`command, the metadata generated would be in
"reST comments" style, which would work only for Nikola.

That means, I needed to modify the metadata, before I can use the exact
same post at both the places. This wasn't too much effort, yet it
involved additional efforts.

Just days later, Nikola released another minor version, and here they
have added functionality that `new_post` would generate the
skeleton in the correct format, based on the configuration setting.

In my particular case, I have added the following like to
`conf.py`

```
METADATA_FORMAT = "Pelican"
```

For more details see
[documentation](https://getnikola.com/handbook.html#metadata-formats)

Currently for this format, only limited metadata headers are generated :
Title (reST style), `type`, `slug` and `date`

The "default" on the other hand have a lot more headers like
`tags`, `category` and more (which I don't
use)

So it would be nice if all the metadata headers were generated.

The way Nikola is making improvements, who knows, this functionality
might just be available in the next release

------------------------------------------------------------------------

*In case it is not clear, I have done exactly what the title says.*
