---
title : "How to Sync Github Fork with the original"
slug : "sync-github-fork"
date : "2018-04-20T19:50:54+05:30"
tags : ["git", "github"]
toc: false
---

First add the original repo as `upstream` remote using :

```shell
git remote add upstream https://github.com/ORIGINAL_OWNER
```

You need to do this only once.

Now each time you want to get the changes from the original, do the following :

```shell
git fetch upstream
git checkout master
git merge upstream/master
```

See the [github documentation](https://help.github.com/articles/syncing-a-fork/)
for details.
