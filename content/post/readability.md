---
title: "Firefox Reader mode in your Terminal"
date: 2022-01-10T06:41:01+05:30
tags: ["terminal"]
categories : ["how-to"]
summary: Do you prefer reading long text in terminal without distracting ads ? Sometimes this helps bypass paywall too 😉
---
As I mentioned in an earlier post, I'm trying to move to more text based workflows.

On that journey, I came across `readable`

`readable` strips out just the readability code from mozilla's firefox engine and provides it as a node module.

`npm install -g readability-cli` will install a script called `readable`

By default, readable will strip out all the non-essentials from the webpage and create another HTML page.

One can not use it as-is, cause it has all the HTML tags. So one needs a browser.

Since we are already in terminal why not use `w3m` ?

Here is how I use it.

`readable https://microblog.desipenguin.com/post/fzf-with-fish/ | w3m -T text/html`

and here is how it looks 😄

![microlog in terminal](/images/microblog-in-terminal.png)

-----

On a related note, w3m official homepage asks one to download tarball.

But your OS's package manager probably has w3m. So I suggest use that instead.

FWIW, I used `brew install w3m` on macOS.
