---
title: "Mutation Tracking in SQLAlchemy"
slug: mutation-tracking"
date: "2016-03-03"
categories: ["misc"]
tags: ["sqlalchemy"]
---

Mutation means change.

Mutation Tracking means ability to detect the change.

The term seemed very complex (to me) at first. But it isn't.

SQLAlchemy ORM depends on the ability to detect the change in the object
so that it can commit it to the DB on `session.commit()`

It is important to understand that if the change is **not** detected,
calling `session.commit()` won't help. (I think) It internally checks
to see if anything is changed. If not, it is (equivalent of) a no-op.

This becomes very important, if you come across bugs like \"why is my
JSON field changes not reflected in the DB\".

For simple data types, the change detection is built-in. But for data
types like `ARRAY` or `JSON` which contain a lot of data **within**
themselves, such a tracking is not built-in.

If you want the ORM to detect such changes, you will to **tell** it. The
`changed` (and other) events must be propagated manually.

In case of `JSON`, one can use `MutableDict` to do that. For `ARRAY`
look at
[this](http://www.kirang.in/2014/08/09/creating-a-mutable-array-data-type-in-sqlalchemy/)
.

Read the official SQLAlchemy documentation about Mutation Tracking
[here](http://docs.sqlalchemy.org/en/latest/orm/extensions/mutable.html).
