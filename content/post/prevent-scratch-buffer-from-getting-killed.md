---
title: "Prevent *scratch* buffer from getting killed"
slug: "prevent-scratch-buffer-from-getting-killed"
date: "2016-09-02"
tags: ["emacs", "spacemacs"]
categories: ["how-to"]
---

I started using `persistent *scratch*` buffer few months
ago. You can read about it [here]({{< relref "post/persistent-scratch.md" >}})

It worked well for some time, and then at least couple of times, I
noticed that when I restart emacs (does not happen very often) my
`*scratch*` buffer is empty.

So I started doing `M-x persistent-scratch-save` every time
I was done writing to `*scratch*`, even though
`persistent-scratch-autosave-mode` was ON

Just recently I realized that I had "lost" my `*scratch*`
buffer while still within my emacs session (It did not show up in the
buffer list) So then I realized why I might be losing the contents. It
is my *guess* that when exiting `persistent-scratch` will
save the contents to disk before exiting, but there was no
`*scratch*` buffer so it creates empty file.

I was under the impression that all the buffers starting with
`*` were special buffers and thus can't be *killed* -
guess I was wrong. I came across the solution on [Stack
overflow](http://stackoverflow.com/a/358740/154947)

I'm listing the relevant code here for your reference :

```lisp
;; bury *scratch* buffer instead of kill it
;; http://stackoverflow.com/a/358740/154947
(defadvice kill-buffer (around kill-buffer-around-advice activate)
  (let ((buffer-to-kill (ad-get-arg 0)))
    (if (equal buffer-to-kill (get-buffer-create "*scratch*"))
        (bury-buffer)
      ad-do-it)))
```
