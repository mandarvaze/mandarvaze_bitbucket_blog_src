---
title : "Change the default Screen shot location for macOS"
tags : [ "micro", "macOS" ]
categories : ["how-to"]
slug : "change-the-default-screen-shot-location-for-macos"
date : "2017-09-16"
description: " If you'd like screen shots to be saved somewhere else, you have to
configure it manually from the terminal."
---

By default, macOS saves all screenshots to the `~/Desktop`.

If you'd like screen shots to be saved somewhere else, you have to
configure it manually from the terminal.

For instance, if you'd like your screenshots to be saved in the
`~/screenshots` directory, then enter the following
commands:

```shell
$ mkdir ~/screenshots
$ defaults write com.apple.screencapture location ~/screenshots
$ killall SystemUIServer
```

[source](http://osxdaily.com/2011/01/26/change-the-screenshot-save-file-location-in-mac-os-x/)
