---
title : "Setup Doom Emacs to work with Vue.js and nvm"
slug : "vuejs-with-doom-emacs-nvm"
date : "2020-02-04T19:40:37+05:30"
tags : ["emacs", "vue", "lsp", "nvm"]
toc: false
---

![Auto Completion](/images/blog/vue-lsp.png)

<!--more-->

Few days ago, I started going thru an excellent Udemy course on Vue.js

You can see my notes [here](https://wiki.desipenguin.com/#Vue.js%20Notes)

On the course, the instructor uses an IntelliJ IDE.
On "Views on Vue" [podcast](https://devchat.tv/views-on-vue/) I had heard good
things about Sarah Dresner extension pack for Vue development. So I tried that.

I worked kinda OK.

I think because it installs a bunch of extensions, they sometimes "fight" with
each other. Other reason may be because I am not too familiar with VSCode, so
keyboard shortcuts that may be obvious to other VS code users, I did not know.

In any case, I needed to setup Emacs in long run, so I came across
[this](https://azzamsa.com/n/vue-emacs/) post.

Additionally, I use `nvm` for my node versions. I anticipated issues because of
that. Luckily adding `(package! nvm)` to `packages.el` was enough.

Here are the final steps for Doom emacs :

1. Add `(package! vue-mode)` to `packages.el`
2. Add `(add-hook 'vue-mode-hook #'lsp!)` to `config.el`
3. Install Vue Language Server : `npm install -g vue-language-server`
4. Finally, let Doom install the new packages via `doom refresh`

I get nice auto-completion, errors and warning popups, as part of LSP.

I also liked that HTML tags are visually paired.

My experience with VS Code was never that smooth.

