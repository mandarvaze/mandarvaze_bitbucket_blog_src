---
title : "Why Keep a Lab Notebook ?"
slug : "why-lab-notebook"
date : "2020-02-28T18:50:08+05:30"
tags : ["advise"]
summary: "How my habit of keeping a developer notebook, (or work diary) helped an
ex-colleague after 6 months ?"
---

## How I Document ##

I have been keeping notes using pen and paper for as long as I remember. But
most of those - at least during the student life - were valuable only during
particular academic year, or so. Usually the notebooks were discarded/lost.

I continued taking notes after I started working, but the nature was "action
items from the meeting". Hence not relevant after a while.

Then I started keeping notes as text files, then markdown, rst, org files. Besides
different markdown syntax, they were all text files that could be easily
searched. Most were kept into a Dropbox folder (or in olden days, backed up to
an external hard drive, when I remembered.)

For a while, I did [devnotes](https://devnotes.desipenguin.com/) website, so that I can
share it with the world.

That site has since been defunct, because I moved to [TiddlyWiki](https://wiki.desipenguin.com/)

I started this as "work diary", hence some initial entries are "what I did that
 day" - I didn't think it would be helpful, but it was. And not only for me,
 others too. (We'll come back to this in a minute)

I've since moved to "notes" format. They have decent title rather than "date"
title, so that it is much easier to guess, especially for visitors. Some of
these turn to slightly formal blog post.

Besides title, TW has decent search and tagging functionality, which helps.

## What do I document ##

Mostly things I learn. If I use then over and over, I do not need to refer to my
notes. 

Sometimes, commands with lots of argument need to be repeated

Sometimes these are steps to spin up a new server, or deploy a product. For the
projects I have worked on, I spin up a new server only once - or twice, in case
we need a separate test/dev server.

These tasks are not so often to justify automation. Just copy/pasting a command
from saved note, is automation enough.

![Automation](https://imgs.xkcd.com/comics/automation.png)

In olden days, I also used to document project specific passwords, till I
started using password manager.

## It Helps (when you least expect) ##

Today a colleague from niteo asked me for a corner case he knew I had worked on.
I knew I had noted it somewhere. So I searched TW, lo and behold. I found it.

The entry was 6 months old.

I knew the context he was looking for, but had forgotten the actual solution.

The issue was related to "pipenv on heroku". Pipenv on heroku is not
upgraded since 2015, this causes many a heart ache. Unless you use "pipenv on
heroku" the problem (and solution) is not of interest to you. (Tweet at me, if
you are REALLY curious)
