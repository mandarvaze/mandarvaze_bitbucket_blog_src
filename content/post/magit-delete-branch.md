---
title : "Doom Emacs: How to Delete a Branch using `magit`"
slug : "magit-delete-branch"
date : "2021-10-07T06:17:37+05:30"
categories : ["TIL"]
tags : ["emacs", "micro"]
summary: "After working on a project for a long time there are lot of stale branches that you should remove periodically. Using some kind of user interface is better than series of git commands. Magit to the rescue!"
---

1. `M-x magit-show-refs` : This shows the list of local as well as remote branches.
2. Go to the `Branches` section (One at the top, most likely you are already there.)
3. Navigate via up/down arrows and select the branch to delete by typing `x` (for `magit-delete-thing`) followed by `RET`
4. In the lower section (mini-buffer) you'll see the list of branches.
5. For unmerged branches you get a confirmation prompt. Type `y` to confirm.

-----

*If you liked this micro-TIL, checkout my other notes at my [Dendron Site](https://pkm.desipenguin.com)*
