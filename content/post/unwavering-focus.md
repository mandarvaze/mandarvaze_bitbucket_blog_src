---
title: "Unwavering Focus"
slug: "unwavering-focus"
date: "2016-05-08"
categories: ["summary"]
tags: ["summary", "self-dev"]
---

I came across Dandapani when tim ferriss mentioned him in one of his
podcasts. Then I looked up his [website](https://dandapani.org/) (very
easy to remember URL, from tim's podcast, at least for someone like
myself of Indian origin)

On his blog, there is a youtube video of his TED Talk from TEDxReno called
"Unwavering Focus".

{{< youtube 4O2JK_94g3Y >}}

I try to summarize the points here.

-   We are told to concentrate (from childhood) yet no one teaches us
    how.
-   We are distracted all the time (13-16 hours x 7 days a week) So we
    become good at "being distracted" (Whatever you practice, makes
    you good at that)
-   Are "internet and smartphones" distractors ? (yes and no)
    -   "Monks with Macs" (and iPhones)
        -   When he became monk, he had to give up everything. All
            "new" monks are given certain attire, set of beads and a
            "MacBook Pro"
        -   "Is it ok for monks to use email ?" "Off course, as long
            as there are no attachments" :)
-   Awareness and Mind
    -   Awareness is like "glowing orb" that moves
    -   Mind is vast plain with various area like anger, jealousy, food,
        sex, science, art.
    -   Wherever the "orb" goes, that part of illuminated, (and you
        become aware)
    -   Art of awareness is to keep the "ball of light/orb" on one
        thing.
    -   "Mind" doesn\'t wander, awareness does.
-   Concentration means "Keep the awareness on one thing for an
    extended period of time"
-   Look for opportunities in daily life
-   In order to teach children to concentrate, parents need to learn it
    first.
