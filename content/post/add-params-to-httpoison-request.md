---
title : "How to add Query Params to HTTPoison.request"
slug : "add-params-to-httpoison-request"
date : "2020-04-10T22:40:39+05:30"
categories : ["how-to"]
tags : ["elixir", "httpoison"]
summary: "`params` are required to append query params to the URL. But how to do that is unclear in the documentation"
---

## Background ##

In case you are not aware, `HTTPoison` is a http client library for Elixir (Like
`requests` in python) I need to pass an `auth_token` as a query param for every
API call.

Unfortunately, (*as of april 2020*) the
[documentation](https://hexdocs.pm/httpoison/HTTPoison.html#request/5) was not
clear enough.

Documentation of this function redirects to
[HTTPoison.Request](https://hexdocs.pm/httpoison/HTTPoison.Request.html) which
does mention `:params` as 

> `:params - Query parameters as a map, keyword, or orddict`

Even the
[example](https://hexdocs.pm/httpoison/HTTPoison.html#request/5-examples)
uses only first 4 of the 5 parameters 😀

Google search also did not help much, until I stumbled upon [this
line](https://github.com/edgurgel/httpoison/pull/37/files#diff-495953187d2b0a519bbbdab02db99d33R80)
in one of the HTTPoison's [PR](https://github.com/edgurgel/httpoison/pull/37)
which I found via one of the searches.

## Solution ##

Send in `params` as part of options like : 

``` elixir

options = [params: [access_token: "my_access_token"]]
HTTPoison.request(
        method,
        url,
        body,
        headers,
        options
      )
```
