---
title : "Pay Gap on the Gender-Blind Platform (Uber)"
slug : "gender-pay-gap-uber-study"
date : "2018-03-07T22:00:19+05:30"
categories : ["podcasts"]
tags : ["freakonomics", "summary"]
description : "Uber's platform does not discriminate based on the Gender. Then what causes the pay gap between male and female drivers ?"
---

Freakonomics podcast did an episode understanding the gender pay gap, that
exists between the male and female driver even if the platform does not treat
them differently based on the gender.

Uber has a **lot** of data about the rides, drivers and earnings.

Let's see what they found out.

## How much ?

Male drivers earn **7% more** than their female counterpart.

> And seven percent is not very different than the overall average we see across
> all firms, even in the traditional labor market.

## Why ?

> what you find is that there are perfectly reasonable explanations for what’s
> happening on the Uber platform.

### Experience

As Uber drivers stay longer with the platform, they start learning the "tricks"
such that they can optimize their earnings.

It was observed that female drivers "leave" lot more frequently and earlier,
than male drivers, and thus do not get the "benefit" of the experience.

> the average man drives about 50 percent more trips per week than the average
> woman, you still have the experience effect for those who have been on the
> platform the same number of months.

### Time of the Day (and location)

Male drivers are more willing to do "graveyard shift". The late night (3AM)
pickups from bars, or drop off to the airports in the "wee" hours, are lucrative
trips.

Female drivers miss out on these opportunities.

> airport trips tend to be the most profitable trips on the Uber platform. So
> what you have is that men tend to complete more airport trips than women

On the other hand, Female drivers (at least partially) make up for these lost
opportunities by being available on a Sunday afternoon, when Men prefer to
"relax at home"

### Speed

It turns out Men driver faster than women, and thus complete more trips in the
given time frame.

More Trips ==> More earning.

> men drive about 2 percent faster than women.

----

### Data used

While the "findings" are more interesting for the most folks, I also found the
amount of data used for this research, amazing.

* Data from January of 2015 to March of 2017
* 1.8 million drivers
* over 740 million Uber trips.
* Focused on one city : Chicago

----

Download/Listen to the Freakonomics Episode #317 and/or read the transcript
[here](http://freakonomics.com/podcast/what-can-uber-teach-us-about-the-gender-pay-gap/)
