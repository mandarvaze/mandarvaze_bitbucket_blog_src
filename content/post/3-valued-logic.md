---
title : "3 Valued Logic"
slug : "3-valued-logic"
date : "2020-12-29T21:32:33+05:30"
categories : ["TIL"]
tags : ["sql"]
summary: "Most programming languages treat NULL as a known value, separate from True and False. SQL does not play by the same rules"
---

![3 Valued Logic](/images/blog/3-valued-logic.png)

Python has clear separation between `True`, `False` and `None`

``` python
$ ipython
Python 3.8.0 (default, Nov 13 2019, 13:19:53)
Type 'copyright', 'credits' or 'license' for more information
IPython 7.18.1 -- An enhanced Interactive Python. Type '?' for help.

In [1]: True == True
Out[1]: True

In [2]: True == False
Out[2]: False

In [3]: False == False
Out[3]: True

In [4]: False == True
Out[4]: False

In [5]: False == None
Out[5]: False

In [6]: None == None
Out[6]: True

In [7]: None == True
Out[7]: False

```

Now look at the similar one from SQL (postgres)

```sql
$ psql -U postgres
psql (12.1)
Type "help" for help.

postgres=# select TRUE = TRUE;
 ?column?
----------
 t
(1 row)

postgres=# select TRUE = FALSE;
 ?column?
----------
 f
(1 row)

postgres=# select TRUE = NULL;
 ?column?
----------

(1 row)

postgres=# select FALSE = NULL;
 ?column?
----------

(1 row)

postgres=# select NULL = NULL;
 ?column?
----------

(1 row)

```

As you might have guessed, psql returns `t` for *True* and `f` for *False*

But when compared to NULL, it does not return either `t` or `f`
In SQL, `NULL` is unknown.

When you think about it, two unknowns are **not** equal.
That is why when we compare `NULL`s - we do not get `t` (As we get when comparing `None`s in python)

-----

*Thanks to [Mo Binni](https://twitter.com/mobinni) for teaching me this via the ZTM DB course on Udemy*
*You should check out [this course](https://www.udemy.com/course/complete-sql-databases-bootcamp-zero-to-mastery)*
