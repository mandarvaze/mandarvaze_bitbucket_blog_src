---
title : "How to Create Header Images for Your Blog"
slug : "images-for-blog"
date : "2020-09-27T06:14:58+05:30"
categories : ["how-to"]
tags : ["blogging"]
summary: "Do you want to be able to include header images for your blog without too much (OK, may be one time) effort ? Read on."
---

![Header Images](/images/blog/images_for_blog.svg)

## Option 1 : Quick and ~~dirty~~ Beautiful ##

Head over to https://coverview.now.sh/ - explore.
It is self explanatory.

When in hurry, this is the best.

It has a nice feature that lets you add an icon based on the technology you are blogging about like python, docker, react, heroku and so on.

While it lets you choose background from bunch of options, you can not (yet) upload your own background image.

## Option 2 : More Work, More Control ##

0. Create a login on Canva. It is free.
1. Create New Design 1600x840 - This size works well for most blogs
2. Go to Background and select something you like
3. Go to Elements, and select twitter. Choose twitter icon that you like
4. Go to Text, select a style and then type in your twitter handle.
5. Go to Text, select a style and then type in your website URL.
6. Go to Text, select "Add Heading", change font to your liking.
7. Arrange the text elements such that Heading is prominent.
8. Optionally, add a little illustration either from Canva or other sites like undraw.co
9. Optionally, add a technology icon like previous option. (I haven't done that yet)

To avoid doing most of these steps over and over, Save background, twitter and website URL in a template. Each time just change the header (and technology icon, if you have one).

I am trying to use SVG for my blog for two reasons - files are "text" and light. But for some reason, my SVG are only Black and White. I'm sure there are color SVG, but then they are as "heavy" as PNG.

I have found Convertio (link in the Resource section below) to be good option. But there are more options out there. (Including `potrace` which one can use locally.)

## Resources ##

* [Canva](https://www.canva.com/join/qlx-tlw-qdh)
* [Undraw](https://undraw.co)
* [Convertio](https://convertio.co/)
