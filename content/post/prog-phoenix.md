---
title : "How a 'request' moves thru various phoenix layers"
slug : "prog-phoenix-1"
date : "2019-05-15T19:47:45+05:30"
categories : ["TIL"]
tags : ["phoenix"]
toc: false
---
How a "request" moves thru various phoenix layers

<!--more-->

``` elixir
connection
|> endpoint()
|> router()
|> pipelines()
|> controller()
```
The `connection` comes in with information about the request.
When Phoenix is done, that `connection` will have the response in it.


*From the book "Programming Phoenix 1.4"*
