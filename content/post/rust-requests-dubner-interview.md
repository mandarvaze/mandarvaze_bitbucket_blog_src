---
title: "Rust, Python Requests Library and Freakonomics"
slug: "rust-requests-freakonomics"
date: "2015-05-25"
tags: ["james-altucher", "changelog", "python"]
categories: ["podcasts"]
---

May 21, 2015
------------

- [Rust Language (The changelog)](https://changelog.com/151/)
  -   safety without garbage collector
  -   single ownership model (so cleanup is easy)
  -   Transfer of ownership (permanent) or lending (temp)
  -   Owner is supposed to cleanup
  -   Better C++
  -   C++ hackers can already do this stuff, but rust enables
      new/web programmers to systems programming
  -   cargo (package management) - all (ex) C++ programmers (who
      now use rust) now like this
  -   cargo - learn from other package managers like ruby, npm
      etc.

------------------------------------------------------------------------

May 22, 2015
------------

- [Talk python to me - Requests library](http://www.talkpythontome.com/episodes/show/6/requests-pycon-and-python-s-future)
  - Kenneth talks about "Hallway" track being most helpful (for
    him?) in pycon.
  - python3 usage spiked significantly since last django release
    used it as default
  - writing all documentation/tutorials to use python3 could
    help adoption
  - Kenneth is happy with python2 personally
  - Kenneth says : "Who are the python3 users that bash everyone
    on HN/reddit - I don't meet them at conferences, These are
    "new kids" who started directly with python3\"
  - Personal favourite projects :
    - docopts - generate options from help string (which
      apparently is an ANSI standard)
    - [Click](http://click.pocoo.org)

------------------------------------------------------------------------

May 23-25, 2015
---------------

- [James Altucher talks to Steve Dubner (Freakonomics)](http://www.jamesaltucher.com/2015/05/dubner/)
  - They talk about latest Freakonomics book, which is
    compilation of various previously published blog posts
  - about 120+ articles curated from 8500 posts
  - Dubner did something similar when he was at the New York times 100th anniv.
    - dubner met his wife (photographer) during this
      assignment
  - Cost of fearing strangers
    -  Most of the damage (attack) done by people you
       *know*
    -  A guy who used to dress as a santa killed his family
       (and himself)
    -  A muslim family was taken off the flight for talking
       about "safest seats on the plane\"
  - Levitt's article about suggestions for terrorists created
    quite a stir on the first (?) day when freakonomics blog
    appeared on NYTimes site ;)
  - terrorists don't need ideas from others, but people worry
  - Stories are important, just the data/results may be boring
    - The Bible is most read book, but only 15% people can
      remember all the 10 commandments
