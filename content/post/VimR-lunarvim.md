---
title : "How to get VimR to use Lunarvim configuration"
slug : "VimR-lunarvim"
date : "2022-02-26T12:53:37+05:30"
categories : ["how-to"]
tags : ["vim"]
summary: Do you like VimR GUI ? Do you like sane default configuration of LunarVim ? Wanna use both these great tools together ?
---

# Backstory

I have been using Lunarvim aka `lvim` from inside the terminal (inside `tmux` to be really specific) for a while now.
It already works the way I want, default keybindings to be specific (What else is there ? Will switch colortheme later)

But opening LunarVim in one of the tmux window, terminal in another, and switching between them became tedious.

When I use Doom Emacs, I use the GUI version of Emacs, as it works better, so I thought I should use one of the GUI for neovim.

I tried neoclide, which didn't even start. `VimR` was better. But I couldn't get it to use LunarVi  configuration which I really liked.

After some searching and asking questions on discord, I came across the fix. The code changes are aavailable in the `rolling` branch of `lvim`
But instructions aren't available. After chasing (harassing ?) the good folks on LunarVim's github issue, I finally got it working 🎉

# The Instructions

These are verbatim instructions from the github PR discussion. See [here](https://github.com/LunarVim/LunarVim/issues/1229#issuecomment-1049624730)

> IMPORTANT: Backup any of these folders if you need to
> 
> Please make sure to run the [uninstaller.sh](https://github.com/LunarVim/LunarVim/blob/ffcfdf43062dd4a3a72eda4a657448354ec55262/utils/installer/uninstall.sh). Then delete all of these neovim folders manually.
>
> ``` shell
> rm -rf "$HOME/.config/nvim"
> rm -rf "$HOME/.local/share/nvim"
> rm -rf "$HOME/.cache/nvim"
> ```
>
> Now open up nvim and check that `:version` is 0.6.1+ and that `:checkhealth` is passing correctly.
> 
> Only after you have verified that your vanilla neovim is working, do the following:
> ```shell
> git clone https://github.com/LunarVim/LunarVim ~/.config/nvim
> touch ~/.config/nvim/config.lua
> ``` 

# Notes

* I downloaded the `uninstaller` script to `/tmp` folder and executed from the terminal as `bash ./uninstaller.sh`
* `:checkhealth` failed initially for me for `escape-time` in `tmux`. While I made the change suggested right there, it did not help. So I started vanilla `nvim` from outside `tmux` and the error went away.
* I also got errors that `python2` and `perl` not found, but they were marked `Optional`, so it didn't matter.


