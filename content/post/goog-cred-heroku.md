---
title : "How to Safely use Google Application Credentials on Heroku"
slug : "goog-cred-heroku"
date : "2019-12-13T23:01:48+05:30"
categories : ["how-to"]
tags : ["heroku"]
summary: "Google expects credentials in a json file. Any file to be deployed to
heroku must be in git. But credentials are sensitive info that should **never**
be in git. So what do we do ?"
---

![Hacker](/images/from_canva/goog-cred-on-heroku.png)

## Problem ##

If you have deployed an app on heroku, you know that the **only** way to get any files there is via git.
Normally this is fine, but Google API expects an environment variable `GOOGLE_APPLICATION_CREDENTIALS` that points to a json **file**.

If you google, you might come across tutorials like
[this](https://towardsdatascience.com/tutorial-google-vision-api-with-python-and-heroku-3b8d3ff5f6ef)
or even [SO
answer](https://stackoverflow.com/questions/57126545/how-to-authenticate-google-vision-api-credentials-on-heroku-using-node-js)

Essentially they tell you to commit the `json` file downloaded from Google into
git. Since heroku does not have a *file system* per se, there is no way to get
the json file to heroku.

**But** ..... this file should **never** be in git. The file contains your
security credentials, and it is (indirectly) linked to your credit card.

Even if hacker does not get (direct) access to your credit card, if they use
*your* credentials, *your* account will get billed (once you go over quota) and
google will charge *your* credit card.

So putting the credentials json file in git is a bad idea.

So what should a developer to do ?

## Solution ##

The *workaround* is to store the contents on json file in an environment variable, and at runtime, create the json file on-the-fly by reading the environment variable.

But there is better option, use this [Custom Build pack](https://elements.heroku.com/buildpacks/elishaterada/heroku-google-application-credentials-buildpack)

Even better, Create `.profile` file in your repo, and create the json file from the
environment variable. [No buildpack required.](https://github.com/elishaterada/heroku-google-application-credentials-buildpack/pull/5#issuecomment-547896688)

### Commands ###


* On your local machine, where you have downloaded the `credentials.json`
  * Store the JSON contents in environment variable :  `heroku config:set GOOGLE_CREDENTIALS="$(< credentials.json)"` 
* Create a `.profile` and put this in it : `echo ${GOOGLE_CREDENTIALS} >
  /app/google-credentials.json`
  * This will create the file on heroku's ephemeral file system each time a dyno
    is booted.
  * Commit this `.profile` in the root of your git repo
* Point to the file as Google API expects.
  * `heroku config:set GOOGLE_APPLICATION_CREDENTIALS=/app/google-credentials.json` 
* 🎉
