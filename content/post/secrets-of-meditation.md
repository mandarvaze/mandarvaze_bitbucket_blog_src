---
title: "Unlocking the secrets of meditation"
slug: "secrets-of-meditation"
date: "2016-05-08"
categories: ["summary"]
tags: ["summary", "self-dev"]
---

This is a summary of the following youtube video by Dandapani

{{< youtube aMzBjtTsGd0 >}}

Unlocking the secrets of Meditation

------------------------------------------------------------------------

Understanding Meditation
========================

- Meditation is not a sweet sauce you add to your life to make it taste good.
  -  Having a carrot in the morning, and the hamburgers, and beers through out
     the day is not a "Healthy" diet. Similarly, meditation in the morning is
     not enough (?)
- First fix your lifestyle that supports meditation
- What is meditation :
  - Prolonged state of concentration
  - Meditation is working with the energy inside of you
- We can't clear your mind, but we could focus the mind.
- Any meditation practice should have a process and a goal
- Meditate on same thing for (at least 5 days a week) before you
  switch, else you'll never make progress.
- To meditate, you need to be able to concentrate.
- How to concentrate :
  - Do one thing for a prolonged time
  - We can **not** multi-task
- Meditation takes courage (to face "ugly" self)
  -   When we start meditating, we face our "ugly" subconscious
  -   we never spent time "cleaning" up the subconscious
- Takes self-acceptance
  -   Accept that who you are today took years, so it will take some years to
      "un-create"
- Just like diet, meditation will take long time before we see results.

Process
=======

-   Start with "1 minute a day" (Cause it is sustainable)
-   Create a daily ritual
-   First thing in the morning
-   Don't interact with anyone (may be shower, coffee/water is fine)
-   Meeting with your energy
  -   Meetings need time and location
-   Sit with spine straight (preferably without back support)
-   Left hand below, right hand on top, thumbs touching each other.
-   If/when you are not concentrating, the tips of thumbs are no longer
    touching and spine is no longer straight.
