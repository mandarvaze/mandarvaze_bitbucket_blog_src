---
title: "writegood mode for emacs"
slug: "writegood-mode-for-emacs"
date: "2016-03-10"
categories: ["explore"]
tags: ["spacemacs", "emacs", "writing", "self-dev"]
---

Today I came across `writegood-mode` for emacs, on one of the reddit thread. I
am not a writer, at least not in "writer of the prose" sense, nor is English my
native language, so I thought it would help me improve my English/writing
skills.

Installing it in `spacemacs` was as easy as adding `writegood-mode` in
`dotspacemacs-additional-packages` and running `SPC f e R`

In order to test it, I used it on one of my [older]({{<
relref "post/why-did-you-speak.md" >}}) *prose* blog post. (I thought it made
sense to run it on a non-technical writeup, although I plan to use it
everywhere, including this post itself)

`writegood-mode` pointed out some usages of *passive voice* (I was going to
initially write this as "a few passive voice usage were pointed out", but
quickly re-thought the sentence to use *active voice*, and avoid weasel word
`few`)

At first, I didn't know what the squiggly lines meant. There was no apparent
help provided. [this page](https://github.com/bnbeckwith/writegood-mode)
provided generic help. As an engineer, I used the "elimination logic". I knew
none of these were weasel words, nor were they duplicates, so the errors must be
about passive voice.

Honestly, I didn't know what it meant (or how to fix it)

Quick googling turned up [this useful
page](https://www.noslangues-ourlanguages.gc.ca/bien-well/fra-eng/grammaire-grammar/voix-voice-eng.html)

------------------------------------------------------------------------

I have not (yet) set a global key to invoke
`writegood-mode`. I am using
`M-x writegood-mode` for now.

`writegood-mode` provides two more features, besides
pointing out errors. There is a grade, and reading ease. When invoked,
they both reported a number. Without comparative data, I have no idea
whether my scores are good/average or just bad.
[here](https://en.wikipedia.org/wiki/Flesch%E2%80%93Kincaid_readability_tests#Flesch_reading_ease)
is some information about the F-K scores.

*Scores for this writeup :*

*Flesch Kincaid grade level score : 4.09* *Flesch Kincaid reading ease
score : 84.83*

------------------------------------------------------------------------

*Edit: 2016-03-14:*

*Added url for the Flesch Kincaid scores*
