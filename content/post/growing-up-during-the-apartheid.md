---
title: "Growing up in South Africa during the Apartheid"
slug: "growing-up-during-the-apartheid"
date: "2017-01-25"
tags: ["podcasts"]
categories: ["podcasts"]
---

*Note: These are not MY experiences*

This week I listened to two podcasts that happen to talk about growing
up in South Africa during the Apartheid. It is one thing to read about
it online, and quite another to hear experiences from someone who was
part of it growing up.

------------------------------------------------------------------------

The first story is that of Susan David, the Author of the book
[Emotional Agility](http://amzn.to/2khqXP0) She recently appeared on the
James Altucher Show ([Episode
203](http://www.jamesaltucher.com/2017/01/susan-david/)) which is where
I heard this story.

Susan was a white female, growing up in South Africa during the
Apartheid. She had a nanny - a black woman - who was very kind to her.
She was Susan's friend, confidant, and "second mother". Susan grew up
in an area designated only for "white" people, the nanny could not
stay with her family. That means she was separated from her own
children.

Once a year, she would travel to meet her family, children for 48-72
hours. She would carry gifts for them, but what was heart wrenching was
that the shoes, or cloths won't fit, since the last time she has seen
her children they were (say) 5 years old, and now they had grown (and
were 6 years old) and thus shoes/cloths won't fit.

------------------------------------------------------------------------

The other story is about Trevor Noah, the new host of the Daily show. He
was recently
[interviewed](http://freakonomics.com/podcast/trevor-noah-lot-say/) on
the Freakonomics Radio podcast.

Trevor was born and raised in South Africa to a black mother, and white
European father. So he was a mixed race kid. As per the Apartheid laws
of that time he was "born a crime" (Thus the name of his
[book](http://amzn.to/2jQsyeJ)). That means, neither of his parents
could openly "own" him.

His mother would pretend to be his "baby sitter" if they were seen
together, and his father would "run away", if he called him "daddy"
in public place.

He says, he was "lucky" to go to a school that was considered a
"white" school.

------------------------------------------------------------------------

To listen to the complete stories, with the full context, listen to the
podcasts. Links given above.
