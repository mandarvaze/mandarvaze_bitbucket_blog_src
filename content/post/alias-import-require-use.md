---
title : "Elixir: Alias, Import, Use and Require"
slug : "alias-import-require-use"
date : "2019-05-27T15:09:12+05:30"
categories : ["TIL"]
tags : ["elixir", "python"]
---
Elixir has four ways to access functionality from other modules

<!--more-->

## `alias`

This allows us to the short version especially for nested name spaces.

`alias MyApp.Accounts.User` let us use just `User` rather than the full
name.

It also allows us to define our own short names. e.g. `alias
MyApp.LattitudeAndLongitude as latlong`

python supports the second use case via `import module as shortname` syntax

## `import`

Import functionality from other modules. This is similar to python's `import`
and `from module import` syntax.

In python, if you want to import a few functions, you use `from module import
func1, func2` etc. But if you want to use **most but not all** functions, you
end up with a long `from` statement.
Elixir provides `only` and `except` options to make such importing easier.

## `use`

`use` **adds** the functionality from other modules into the current one.

```
defmodule Module2 do
  def func2 do
    IP.puts "func2"
  end
end

defmodule Module1 do
  use Module2
end

Module1.func2
```

Here, `func2` became available to `Module1`

python does not have similar functionality.

## `require`

This is similar to `use` except for macros. It makes a macro from an external
module available to the compiler at compile time.

------

* References

* [elixir
  documentation](https://elixir-lang.org/getting-started/alias-require-and-import.html)
* [elixir examples](https://elixir-examples.github.io/examples/alias-use-import-require)

