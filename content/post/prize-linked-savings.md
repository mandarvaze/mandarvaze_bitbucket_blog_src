---
title: "Prize Linked Savings Account"
slug: "prize-linked-savings"
date: "2017-12-13"
tags: ["podcasts", "freakonomics"]
categories: ["podcasts"]
---

![Prize Linked Savings](/images/blog/prize-linked-savings.png)

Recently I heard an episode on Freakonomics podcast called "No-lose
Lottery". You can listen to the episode
[here](http://freakonomics.com/podcast/say-no-no-lose-lottery-rebroadcast/)

Apparently, this is a rebroadcast. Original episode was in 2010.

But those details aside, this post is to quickly summarize the concept
of "Prize Linked Savings Account"
<!--more-->

As shown in the illustration above,

The savings accounts provide you with the safety. That is *your* funds
are always safe, and you also get some interest.

This interest may not be much, in fact there are better avenues of
investment.

But each higher "potential" revenue instrument comes with higher risk.

Highest risk (and possible highest reward) comes from a lottery.

You could win a jackpot.

But most people don't.

They keep on "investing" in the hopes of winning "big" (and in the
process, lose their savings)

Prize Linked Savings accounts bring the best of both worlds.

It is a savings account (so your money is safe) But the interest rate is
**zero**

One doesn't earn anything on that money. (but like other bank account,
you can take out your money any time)

But ...

There is a potential to earn "big"

Every so often there will be a winner.

MAMA - Millionaire-A-Month-Account, as the name suggests has a "draw"
once every month.

Seems like a good idea, win-win, no ?

But then there come the regulators, the bureaucrats

They don't like it (for the most part, although that is changing)

Most of the times, the "lottery" industry is monopolized by the
government. There is a huge revenue in this industry, so government does
not want to "share the pie"

And this is not geography specific.

The podcast provides examples from US as well as South Africa.

So what can you, if you liked this idea ?

I guess try to "influence" your policy makers to allow such bank
accounts ;)
