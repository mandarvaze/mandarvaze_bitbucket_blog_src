---
title: "Habit Loop"
slug: "habit-loop"
date: "2016-06-15"
categories: ["book"]
tags: ["summary", "self-dev"]
---
I'm reading a book [The Power of Habit](http://amzn.to/1YyCLeU) by
Charles Duhigg

So far I am fascinated by the information. Writing style is quite
engaging.

In first few chapters he talks about "Habit loop"

> Cue -> Routine -> Reward

.... and suddenly I started seeing "habits" everywhere

Two habits I want to mention here are of my children

------------------------------------------------------------------------

A younger one, just about an year old, throws tantrums when we are
changing his nappy or putting on a diaper. But I started playing
peek-a-boo with him when he is lying on his back, and he closes his eyes
and plays peek-a-boo since he enjoys that, he lays "still", till I put
on a fresh diaper

> Cue : Lay on the back watching father
>
> Routine : Lay still/put hands on eyes and play peek-a-boo
>
> Reward : Fun !!

------------------------------------------------------------------------

His older brother is about 3 yrs old. He doesn't want to leave TV and
go to sleep. Recently I showed him "shadow play" where all his toy
animals appear before "torch", then their shadows do something
"fun", and jump on the bed and sleep. He really enjoys this.

.... and at the end, after all the animals have "gone to sleep", he
is the last one to look at his own shadow, wave, and jump on the bed to
sleep.

He likes this so much, that I just have to "remind" him of the
"shadow play" and he will be OK me switching off the TV

> Cue: Reminder about "upcoming" shadow play
>
> Routine: brush his teeth, a mini-bath, putting on night dress
>
> Reward : Shadow play

------------------------------------------------------------------------

Now, I need to look at my own life to see the habit-loop
