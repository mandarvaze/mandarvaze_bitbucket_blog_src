---
title : "How to run Chrome from Docker in macOS"
slug : "chrome-on-docker"
Date : "2020-05-16T07:20:23+05:30"
categories : ["how-to"]
tags : ["docker", "macOS"]
summary: "Do you need access to Chrome only occasionally ? Would you prefer not installing it locally ? If yes, then read on."
---

![Chrome from Docker](/images/blog/chrome_from_docker.png)

## Background ##

Some days ago, I got a call from someone asking for a solution. Due to global
pandemic they were unable to hand over a laptop to new employee. They wanted
the new employee to access a certain websites but did not wish to share
the passwords.

Their idea : provide access to *already-logged-in* browser to the new
person, so they would have access to the sites, but do not need the passwords.

They had considered AWS workspaces - but it is costly. (Finally they got some
"Free for a couple of months" deal, and used as their final solution)

I suggested "Chrome in Docker". The conversation ended, but it intrigued me and
wanted to try it out myself.

I started with
[this](https://github.com/jessfraz/dockerfiles/tree/master/chrome/stable) 
but it did not work as-is for me. Partially because (I think) the original repo
assumes linux host, and I am on macOS. So I had to make some changes.

## Summary of my changes ##

* I included the `chrome.json` in the repo. See details about this file in
  the Dockerfile comments.
* I had to replace `libpango` with `libpangox` since the original repo is 
  couple years old.
* Removed `talk-plugin` cause I don't need it.
* Created `run.sh`- because the `docker` command becomes too long, and
  changing a param becomes PITA
* Removed `--device` params because it does not apply to macOS (I think)
* Finally, I needed XServer, which comes with linux but not macOS. Hence
  `brew cask install xquartz`

## Bit of Troubleshooting ##

Where is the fun if things work in the first attempt ?
I kept getting `Unable to Open XDisplay` error.

To fix the error, I had to set the `ip` as per
[this](https://medium.com/@dimitris.kapanidis/running-gui-apps-in-docker-containers-3bd25efa862a)

I created `setxhost.sh` to simplify this. (Originally it was part of `run.sh`)

You can see all my changes in my
[git repo](https://github.com/mandarvaze/chrome-on-docker) which contains all my
modification.

The README has detailed instructions to try this on your mac.
