---
title: "(ya)snippet for header in org-mode blog post"
slug: "snippet-org-blog-header"
date: "2016-03-11"
categories: ["misc"]
tags: ["blog", "emacs", "spacemacs"]
---

*I don't know how much sense does the title make. But I didn't know
how else to express, plus (I) didn't want to wait to figure out the
title*

------------------------------------------------------------------------

As mentioned earlier, I am using nikola for this blog, and posts are in
`org-mode` as much as possible.

For every post in nikola, I need to add meta data information like
`title`, `slug`, `date` etc.

Initially I created first one by hand, and copy pasted from older posts.

Then I decided to automate it using powerful `yasnippet`

Creating a new snippet was easy I just invoked `M-x yas-new-snippet` and
followed the prompts.

I created it only for `org-mode` though. I think it will be useful for
posts in ReST as well (I just need to remove `#+BEGIN_COMMENT` and
`+END_COMMENT`

Here is the (a simple) snippet file :

    # -*- mode: snippet -*-
    # name: begin and end comment block for blog posts in org mode
    # key: bc
    # --

    #+BEGIN_COMMENT
    .. title:
    .. slug:
    .. date:
    .. category: misc
    .. tags:
    .. type: text
    #+END_COMMENT

------------------------------------------------------------------------

emacs stored the file at
`~/.emacs.d/private/snippets/org-mode/blog_headers`
