---
title : "Open Source Contribution: flake8 documentation"
slug : "oss-contri-flake8-doc-update"
date : "2019-06-07T17:09:08+05:30"
categories : ["TIL"]
tags : ["doc", "oss", "contribution"]
---


Recently I did a couple of open source contributions. I decided to document my
learning here.

In this pull request I updated the contributor documentation  for `flake8`
<!--more-->

## Project

For anyone who is working in python for a while is already familiar with
`flake8` It could be described as a "linter" that checks style and quality of
python code.

Officially it describes itself as :

> a python tool that glues together pep8, pyflakes, mccabe, and third-party plugins to check the style and quality of some python code.

## The Change

The change was minor documentation change asking contributor to *not* include
`.gitignore` in their PR.
The documentation gives them option to use a global `.gitignore` instead.

## What I learnt

I did not know about either of the alternatives. Global `.gitignore` or
`.git/info/exclude`
I also used reStructuredText (`.rst`) after a long time. Had to struggle a little
bit with muscle memory of using markdown.

I also got to brush up on sphinx. I had used it a while ago to generate my
project's documentation.

## The Pull Request

For your reference, you can see the PR itself 
[here](https://gitlab.com/pycqa/flake8/merge_requests/323)
