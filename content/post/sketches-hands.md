---
title: "Sketches : Hands"
slug: "sketches-hands"
date: "2017-01-09"
tags: ["podcasts"]
categories: ["podcasts"]
---

*This post comes from a podcast episode called "Fool me once". So the
title of* *this post is apt. The post does not contain a drawing of a
hand. ;)*

------------------------------------------------------------------------

I recently started listening to a new podcast called "Tell me something
I don't know". [TMSIDK](http://tmsidk.com/) for short.

In this episode one of the contestant, A.E. Kieren , tells us that
"hands" are conspicuously missing from the Art of the renaissance
period.

The reason being that hands are very difficult to draw, and hence the
artist would "charge" extra if the "paid" assignment required the
hands to be included.

On a lighter note, he suggest that now onwards, if you see a portrait
from the renaissance period without hands, feel free to assume that the
person who commissioned that portrait was "too cheap to pay extra for
the hands" 😉

A.E.Kieren is an artist himself. He drew sketches of the judges of this
episode, which you can see on the episode page. He mentions that during
his college days, his teacher told him "hands" were his weak spot, in
front of the whole class So he created a separate sketchbook just for
"hands" and practiced "hands" relentlessly, each day.
