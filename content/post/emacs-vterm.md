---
title : "How to use Emacs as an IDE with Integrated Terminal"
slug : "emacs-vterm"
date : "2021-12-02T05:48:17+05:30"
categories : ["how-to"]
tags : ["emacs"]
summary : "I spend my time in either Emacs and Terminal. I tried to use `shell-mode` in emacs and got all sorts of wonky characters. Till I found `vterm`."
---

*Note: *Following works for Doom Emacs. But with minor change, it should work for vanilla emacs as well*

* Make sure you have `CMake` installed.
  * On macOS : `brew install cmake`
  * On Ubuntu based linux distros, it should be `sudo apt-get install cmake`
  * Otherwise use `dnf` or `pacman` or appropriate command for your linux distro.
* Enable `vterm` in `init.el`
* `doom/reload`
* This will install appropriate package, and will install and compile `libvterm`
* Starting a Terminal is as simple as `M-x vterm`
* I am running `tmux` inside of `vterm` to get multiple terminals running in different working directories.😄

Issues:
* I'm yet to figure out how to use `SPC` leader key without sending space character to the Terminal.
  * Workaround: I use mouse to click on non-terminal buffer. 😜
* As my normal workflow, I used `git commit` from inside `vterm` where my default editor is vi/vim/nvim. When I type `:wq` to save the `COMMIT_MSG` Emacs intercepts the keystrokes, rather than sending to `vi` running inside `vterm` running inside Emacs. 😆
  * Solution: Use Magit for the commits, rather than `vterm`
