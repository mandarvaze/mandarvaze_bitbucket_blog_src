+++
title       = "Please check your gas amount"
tags        = ["ethereum", "solidity"]
slug        = "please-check-your-gas-amount-maybe-misleading"
date        = "2018-02-09"
categories  = ["blockchain"]
description = "How I solved this - seemingly misleading error - from my Smart Contract"
+++

I just started learning solidity development, and when I started writing my own
Smart Contract - I came across this error.

``` shell
Error encountered, bailing. Network state unknown. Review successful transactions manually.
Error: The contract code couldn't be stored, please check your gas amount.
```

`truffle compile` worked OK, but `truffle migrate` gave this error.

Usually, it is said that is there is a loop (infinite loop, due to logic error)
one usually "runs out of gas"

If you purely look at the error, it seems "logical" i.e. there is no compilation
error, but when deploying the smart contract you "ran out of gas"

But I was writing a really simple code - there were no loops.

Over the years, I have realized that sometimes, reviewing the code helps uncover
the problem, so I did. Without looking for "loops" and such, and realized that
the problem was not really runtime error.

In my opinion, this should have been caught at compile time.

To understand what was the problem, let's see some code

One thing different from usual "hello world" code was that I had two classes.

``` javascript

    contract Animal {
        uint256 public weight;
        function name() public view returns (string);
    }
```
and

``` javascript

    contract Dog is Animal {
       ....
       ....
       ....
    }
```

Obviously `Animal` is an interface, and method `name()` must be
implemented by `Dog`

It is obvious looking back.

But when you are shown an error "please check your gas amount" - you may be
thrown off guard.

Once I added the missing method, the error went away.

I understand solidity is a new language, and the ecosystem is yet maturing. I'm
sure, eventually the error messages will become more clearer/precise or just
overall "better"

In the mean time, **Review your code without any bias, and you may just find the
solution.**
