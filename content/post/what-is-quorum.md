---
title : "Quorum: Permissioned Ethereum Blockchain"
slug : "what-is-quorum"
date : "2018-10-30T16:17:21+05:30"
categories : ["blockchain"]
tags : ["ethereum", "quorum"]
---
Quorum is an Ethereum-based distributed ledger protocol that has been developed
for a permissioned implementation of Ethereum to support transaction and
contract privacy.
<!--more-->

# Features

* Private/Permissioned blockchain, based on `geth` (Go implementation of Ethereum)
* Performance: upto 100s of TPS
* Focus on privacy - Reveal transactions only to those who are party to the
transaction.

## Ethereum compatible

Quorum is a fork of ethereum itself. So it is much easier to "port" the smart
contracts from Ethereum to Quorum.
All the knowledge of Solidity and EVM can be carried over.

Quorum will be kept up to date with future `geth` releases.

## Permissioned
Permissioned network means that the node will not "talk" to any random node on
the network.

When `--permissioned` flag is passed when starting the node, it
looks for a "white list" of nodes, that this node will accept incoming
connections from and make outgoing connections to.

This mean we have explicit control over which node we allow in our network.

The nodes also have roles "maker" and "voter". As the name suggests, only
"maker" can create new blocks.

## Privacy
How does Quorum handle privacy ? By dividing the information
into two : public and private. Please remember that public and private does not
refer to public blockchain. It refers to whether the information is accessible
to everyone (thus "public") or not ("private")

Every node does **not** have full state DB. Only "admin" and "regulator" node
have full DB. regulator node is a node that is party to **all** transactions,
that is why it has complete private state DB.

Each node:

* has 2 DBs - Public state, and private state.
* **validates** all **public** transactions
* **Execute smart contract code** for **private** transactions where they are
party.
* **Skip smart contract code execution** for "other" private transactions.

Additionally, "private smart contract can **not** change public state"
(Nor can it create public contract *i.e. Once private always private. Switching
sides not allowed.*)

## Performance

Since this is a permissioned private blockchain, there is no need for PoW. So
using other consensus algorithms, Quorum is able to achieve better performance.

Voting based consensus algorithm (`QuorumChain`) Implemented as a smart
contract. Thus allowing to *upgrade* the consensus.

# References

* [Quorum Whitepaper](https://github.com/jpmorganchase/quorum-docs/blob/master/Quorum%20Whitepaper%20v0.1.pdf)
* [Quorum Wiki](https://github.com/jpmorganchase/quorum/wiki)
