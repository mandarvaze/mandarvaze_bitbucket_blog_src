---
title: "Sending email via emacs"
slug: "send-email-from-emacs"
date: "2016-03-20"
categories: ["how-to"]
tags: ["emacs", "spacemacs", "email"]
---

*Note : This post was originally written in `org-mode`. When I moved to `hugo`
I "translated" in to `markdown` (via `pandoc`). That is why you see some
TODO/DONE statuses below*

-----

I have been meaning to setup emacs for email in general, but due to the
size of my existing mailbox, setting up `offlineimap` (or `mbsync` for
that matter) is a daunting task. I am sure I'll do that one day. But
for now, I wanted to start small, or as Sacha Chua says "baby steps"

So I decided to setup sending an email first, and then slowly move to
using programs like `mu4e` to read/search my emails.

I referenced [this]
(http://sachachua.com/notebook/wickedcool/wc-emacs-05-gnus.html)
 page to get started. But it did not work as-is.

I had to add a line (see below) to get SMTP server using ssl Also, the
line starting `smtpmail-auth-credentials` didn't work.

Luckily, after adding the `ssl` line, I got interactive prompt asking
for username and password. This was saved into a `~/.authinfo` file.
unfortunately the password is stored in plain-text. I know there are
ways to encrypt it using `gpg`, just that I haven't explored it yet.

Here is the relevant part of my config

```lisp
;;http://sachachua.com/notebook/wickedcool/wc-emacs-05-gnus.html
(setq smtpmail-smtp-server "smtp.example.com")
(setq smtpmail-stream-type  'ssl)
(setq smtpmail-smtp-service 465)
(setq send-mail-function 'smtpmail-send-it)
(setq message-send-mail-function 'smtpmail-send-it)
```

This was the *minimal* setup without using any additional tools.
Everything came built-in.

TODOs
=====

✔ DONE Fix the default "From" prompt.
---------------------------------------

*CLOSED: 2016-03-20*

This was achieved using the following configuration.

```lisp
 ;;https://www.emacswiki.org/emacs/SendingMail
(setq user-full-name "Mandar Vaze")
(setq smtpmail-local-domain "example.com")
(setq user-mail-address (concat "mandar.vaze@" smtpmail-local-domain))
```

✔ DONE encrypt the password
---------------------------

*CLOSED: 2016-03-21*

emacs has ability to auto encrypt and decrypt files with `.gpg`
extension. `EasyPG` is part of Emacs 23+, but did not work for me on
OSX.

Found a solution
[here](http://danzorx.tumblr.com/post/11976550618/easypg-for-emacs-on-os-x-or-sometimes-emacs).

`brew install gpg`

I did not have to fiddle with `PATH` though.

I already had a `.authinfo` created by `gnus` I just saved it as
`.authinfo.gpg`. I followed the prompts and was easily able to encrypt
the file on the disk.

When I sent an email, I was asked for the passphrase (as expected) that
was all.

✔ DONE Read emails via emacs
----------------------------

*CLOSED: 2016-03-21*

This deserves a separate post in itself. There is so much explore/learn
(for me) and using `gnus` in itself may take up good part of it, cause
it is unlike other email clients I have used.

But quick summary of what worked for me:

```lisp
;; http://www.mostlymaths.net/2010/12/emacs-30-day-challenge-using-gnus-to.html
(setq gnus-select-method '(nnimap "domain"
                          (nnimap-address "imap.example.com")
                          (nnimap-server-port 993)
                          (nnimap-stream ssl)))
```

I also had to add a new entry in the `.authinfo.gpg` for
`imap.example.com` because the machine name is different as well as the
port number.

The very first attempt failed, because I failed to notice that I was
being asked a passphrase for my `.authinfo.gpg` file. But one that is
provided, it worked well.

Off course, I did not have anything *unread*, so I was presented with a
empty screen. So I went to the web version of the email, and marked a
few threads as *unread*. and **now** I was presented with `INBOX`

Navigating is a bit awkward. But to be honest, at this point I was happy
that emails showed up.

It will be a while before I can start "using" emacs as a mail client.
There is so much to do :

-   offline IMAP
-   setting up better mail client like `mu4e`
-   integrating with `bbdb` for contacts integration
-   ability to read HTML emails

---------

Update : Feb 27, 2018

3 out of 4 were done long ago. I did not need `bbdb` integration.
